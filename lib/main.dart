
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:call_log/call_log.dart';
import 'package:whatsub/phone_log.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:share/share.dart';
import 'package:launch_review/launch_review.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';




const String contactsBoxName = "contacts";

@HiveType(typeId: 0)
class Contact {
  @HiveField(0)
  String phoneNumber;

  Contact(this.phoneNumber,);
}


void main() async{
  await Hive.initFlutter();
  Hive.registerAdapter<Contact>(ContactAdapter());
  await Hive.openBox<Contact>(contactsBoxName);
  runApp(CupertinoStoreApp());

}
class CupertinoStoreApp extends StatefulWidget {
  @override
  _CupertinoStoreAppState createState() => _CupertinoStoreAppState();
}

class _CupertinoStoreAppState extends State<CupertinoStoreApp> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.green
        ),
        home:DefaultTabController(length: 2, child: Pure())

    );
  }
}

class Pure extends StatefulWidget {


  @override
  _PureState createState() => _PureState();
}

class _PureState extends State<Pure> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


  String phoneNumber;
  String code;
  _displayDialog(BuildContext context) async {
    bool _save=true;
    return showDialog(
        context: context,
        builder: (_){
          return Dialog(
            child: Container(
              padding: EdgeInsets.all(20.0),
              height: 275.0,
              width: 300.0,
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CountryCodePicker(
                    enabled: true,
                    showCountryOnly: true,
                    alignLeft: true,
                    initialSelection: 'in',
                    favorite: ['+91','in'],
                    onInit: (CountryCode countryCode){
                      code=countryCode.toString();
                    },
                    onChanged: (CountryCode countryCode){
                      code=countryCode.toString();
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(

                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Colors.green)
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(color: Colors.green)
                        ),
                        filled: true,
                        fillColor: Colors.grey[100],
                        hintText: "Phone Number"
                    ),
                    keyboardType: TextInputType.phone,
                    initialValue: "",

                    onChanged: (value) {
                      setState(() {
                        phoneNumber = value;
                      });
                    },
                  ),
                  SizedBox(height: 10.0,),
                  Row(children: <Widget>[
                    alert(),

                    SizedBox(width: 5.0,),

                    Text('Save for Later'),
                  ],

                  ),


                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(onPressed:(){
                        Navigator.pop(context);
                      }, child: Text('CANCEL',style: TextStyle(color: Colors.green),)),
                      FlatButton(onPressed: (){
                        FlutterOpenWhatsapp.sendSingleMessage(code+phoneNumber, "");
                        if(_save==true){
                          Box<Contact> contactsBox = Hive.box<Contact>(contactsBoxName);
                          contactsBox.add(Contact(code+phoneNumber));
                          _save=false;
                        }

                        Navigator.of(context).pop();
                      }, child: Text('OPEN CHAT',style: TextStyle(color: Colors.green),))
                    ],
                  )
                ],
              ) ,
            ),
          );
        }

    );


  }

  String _value;
  @override
  Widget build(BuildContext context) {

    return Scaffold(

        appBar: AppBar(
          title: Text('Open WhatsApp',style: TextStyle(fontFamily: 'rubik'),),
          backgroundColor: Colors.green,
          actions: <Widget>[
            PopupMenuButton(

              padding: EdgeInsets.all(10.0),
              itemBuilder: (context) => [
                PopupMenuItem(
                    value: 'one',
                    child:
                    Row(
                      children: <Widget>[
                        Icon(Icons.favorite,color: Colors.green,),
                        SizedBox(width: 5.0,),
                        Text('Rate App'),
                      ],
                    )
                ),

                PopupMenuItem(
                    value: 'two',
                    child:
                    Row(
                      children: <Widget>[
                        Icon(Icons.share,color: Colors.green,),
                        SizedBox(width: 5.0,),
                        Text('Share'),
                      ],
                    )

                ),
                PopupMenuItem(
                    value: 'three',
                    child:
                    Row(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.bug,color: Colors.green,),
                        SizedBox(width: 5.0,),
                        Text('Report Issue'),
                      ],
                    )
                  // ListTile(
                  //   leading: Icon(FontAwesomeIcons.bug),
                  //   title: Text('Report Issue'),
                  // )
                ),
              ],
              onSelected: (value){
                if(value=='one')
                {
                  LaunchReview.launch(
                    androidAppId: "com.magnolabs.whatsapp2",

                  );
                }
                else if(value=='two')
                {
                  Share.share('https://play.google.com/store/apps/details?id=com.magnolabs.whatsapp2'
                      '');
                }
                else if(value=='three')
                {
                  FlutterOpenWhatsapp.sendSingleMessage('+91 8129350774', "");
                }
              },
            )


          ],
          bottom: TabBar(tabs: [
            Tab(child: Text('Recent'),),
            Tab(child: Text('Call log'),)
          ]),
        ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.dialpad),
            onPressed: (){
              setState(() {
                _displayDialog(context);

              });
            }

        ),
        body: TabBarView(children: [
          ValueListenableBuilder(
            valueListenable: Hive.box<Contact>(contactsBoxName).listenable(),
            builder: (context, Box<Contact> box, _) {
              if (box.values.isEmpty)
                return Center(
                  child: Text("No contacts"),
                );
              return ListView.builder(
                itemCount: box.values.length,
                itemBuilder: (context, index) {
                  Contact currentContact = box.getAt(index);
                  return Card(
                    child: ListTile(
                      leading: IconButton(icon: Icon(FontAwesomeIcons.whatsapp,
                        color: Colors.green,size: 30.0,), onPressed:()
                      {
                        FlutterOpenWhatsapp.sendSingleMessage(currentContact.phoneNumber, "");
                      }
                      ),
                      title: Text(currentContact.phoneNumber,
                        style: TextStyle(fontFamily: 'monospace',
                            fontSize: 20.0),),
                      trailing: IconButton(icon: Icon(Icons.delete,
                        color: Colors.red,size: 30.0,), onPressed: ()
                      {
                        return showDialog<void>(
                          context: context,
                          barrierDismissible: false, // user must tap button!
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content: Text('Are you sure want to delete this item?'),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('CANCEL'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text('OK'),
                                  onPressed: () async {
                                    await box.deleteAt(index);
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }),
                    ),
                  );
                },
              );
            },
          ),
          MyApp()
        ])
    );
  }
}


class alert extends StatefulWidget {
  @override
  _alertState createState() => _alertState();
}

class _alertState extends State<alert> {
  bool _save=false;
  @override
  Widget build(BuildContext context) {
    return Checkbox(value: _save, onChanged: ( bool value){
      setState(() {
        _save=value;

      });
      // save=false;
    },);
  }
}







class ContactAdapter extends TypeAdapter<Contact> {
  @override
  final typeId = 0;

  @override
  Contact read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Contact(
      fields[0] as String,

    );
  }

  @override
  void write(BinaryWriter writer, Contact obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.phoneNumber);

  }
}











