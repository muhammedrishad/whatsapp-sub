import 'package:call_log/call_log.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:call_number/call_number.dart';



class MyApp extends StatefulWidget {
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget joy;
  Future resi()async{

    final Iterable<CallLogEntry> rishad=await CallLog.query(
      dateFrom:  DateTime.now().subtract(Duration(days: 2)).millisecondsSinceEpoch,
      dateTo:  DateTime.now().subtract(Duration(days: 0)).millisecondsSinceEpoch,



    ).catchError((e){debugPrint(e.toString());});
    debugPrint("res length "+rishad.length.toString());
    return rishad;

  }


  @override
  Widget build(BuildContext context) {
    // var yesterday=DateFormat('dd').format(DateTime.now().add(Duration(days: -1)));
    // int yes=int.parse(yesterday);

    Iterable<CallLogEntry> entries;
    // var dt=formatDate(DateTime.now(), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn]);


    return SafeArea(
      // color: Colors.black,
      // padding: EdgeInsets.all(0.0),
      child: FutureBuilder(
          future: resi(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());
            else if(snapshot.hasData){
              entries=snapshot.data;

              var children = <Widget>[];

              entries.forEach((element) {children.add(

                  Card(
                      child: Padding(

                          padding: EdgeInsets.symmetric(horizontal: 20.0),
                          child: ListTile(

                            leading:IconButton(
                              icon:  Icon(FontAwesomeIcons.whatsapp,color: Colors.green,size: 30.0,
                              ), onPressed: (){
                              FlutterOpenWhatsapp.sendSingleMessage(element.number, "");
                            },),
                            title: Text(element.number,style: TextStyle(fontSize: 17.0,fontFamily: 'rubik'),),
                            subtitle:
                            Text(element.name,style: TextStyle(fontFamily: "rubik",),
                              textAlign: TextAlign.left,),

                            trailing: IconButton(icon: Icon(Icons.call,color: Colors.green,size: 30.0,),
                                onPressed: ()async {
                                  if (element.number != null)
                                    await new CallNumber().callNumber(element.number);
                                }
                            ),
                          ))));
              });
              return ListView(children: children,);
            }

            return Center(child: Text('Error'));
          }),
    );
  }
}






